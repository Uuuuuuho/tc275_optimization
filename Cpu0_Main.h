#include "Ifx_Types.h"
#include "IfxCpu.h"
#include "IfxScuWdt.h"
#include "Potentiometer.h"
#include "BasicStm.h"
#include "Configuration.h"
#include "Ultra_RGB.h"
#include "LED_Buzzer.h"

//===========================================
/* ultrasonic + RGB */
unsigned int timer_cnt;
unsigned int start_time;
unsigned int end_time;
unsigned int interval_time;
extern unsigned int distance;
//===========================================

#define BUTTON_SW1  &MODULE_P02,0   /* Port pin for the button  */
#define BUTTON_SW2  &MODULE_P02,1   /* Port pin for the button  */
//===========================================


/* ISR Definition */
__interrupt( 0x0A ) __vector_table( 0 )
void CCU60_T12_ISR(void)
{
    timer_cnt++;

    if(timer_cnt == 1)
    {
        /* Set TRIG Pin */
        PORT15_OMR |= (1<<PS4);
    }
    else if(timer_cnt == 2)
    {
        /* Clear TRIG Pin */
        /* Generate 10us Pulse */
        PORT15_OMR |= (1<<PCL4);
    }
    else if(timer_cnt == 10000)
    {
        /* TRIG Period: 100ms */
        timer_cnt = 0;
    }
}

__interrupt( 0x0B ) __vector_table( 0 )
void ERU0_ISR(void)
{
    if((PORT15_IN & (1<<P5)) == 0)              // Falling edge
    {
        /* Get distance */
        end_time = (timer_cnt * 500) + CCU60_T12;

        interval_time = end_time - start_time;  // clock per 0.02us

        distance = (interval_time * 20) / 58000; // unit: cm

    }
    else                                        // Rising edge
    {
        start_time = (timer_cnt * 500) + CCU60_T12;
    }
}