#ifndef APPTASKFU_H_
#define APPTASKFU_H_

#include <Ifx_Types.h>
#include <stdio.h>
#include "BasicStm.h"
#include "Configuration.h"
#include "Potentiometer.h"
#include "Ultra_RGB.h"
#include "LED_Buzzer.h"
#include <string.h>

IFX_EXTERN boolean task_flag_1m;
IFX_EXTERN boolean task_flag_10m;
IFX_EXTERN boolean task_flag_100m;
IFX_EXTERN boolean task_flag_1000m;


/* ************************ */
/* ----- Declaration ------ */
/* ************************ */
void convolution();
void ultraRGB(void);
void LED_Buzzer(void);


/* **************************** */
/* ----- Global variable ------ */
/* **************************** */
extern Basic_Stm g_Stm;




#if MAT_LOC == GLOBAL
#if MATRIX == INTEGER
// static volatile int input[IN_CH][M][N];
// input = (*(volatile unsigned int*)(0x70000000));
static int input[IN_CH][M][N];
static int kernel[KERNEL_CH][P][Q];
static int output[OUTPUT_CH][OUTPUT_X][OUTPUT_Y];
#elif MATRIX == FLOAT
static float input[IN_CH][M][N];
static float kernel[KERNEL_CH][P][Q];
static float output[OUTPUT_CH][OUTPUT_X][OUTPUT_Y];
#endif          /* matrix data type */
#endif          /* matrix location */


/* ********************************* */
/* ----- Function declaration ------ */
/* ********************************* */
void appTaskfu_init(void);
void appTaskfu_1ms(void);
void appTaskfu_10ms(void);
void appTaskfu_100ms(void);
void appTaskfu_1000ms(void);
void appTaskfu_idle(void);
void appIsrCb_1ms(void);
void init_TCM(void);
#endif /* APPTASKFU_H_ */
