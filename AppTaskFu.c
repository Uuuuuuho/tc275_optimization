#include "AppTaskFu.h"


#define BUTTON_SW1  &MODULE_P02,0   /* Port pin for the button  */
#define BUTTON_SW2  &MODULE_P02,1   /* Port pin for the button  */

static sint32 task_cnt_1m = 0;
static sint32 task_cnt_10m = 0;
static sint32 task_cnt_100m = 0;
static sint32 task_cnt_1000m = 0;

boolean task_flag_1m = FALSE;
boolean task_flag_10m = FALSE;
boolean task_flag_100m = FALSE;
boolean task_flag_1000m = FALSE;


/* value of the potentiometer sensor */
volatile Ifx_VADC_RES g_conversionResult;

/* LED BUZZER PURPOSE */
volatile int BUTTON1_State = 0;
volatile int BUTTON2_State = 0;

void init_TCM(void){
#if MAT_LOC == GLOBAL	
#if TILING == TRUE
	memmove(0x70000000,input,sizeof(input));
	// memcpy(0x70000000,input,sizeof(input));
	// input = (*(volatile unsigned int*)(0x70000000));
#endif			/* tiling */
#endif			/* matrix location */

}

void appTaskfu_init(void){
}

void appTaskfu_1ms(void)
{
	task_cnt_1m++;
	if(task_cnt_1m == 1000){
		task_cnt_1m = 0;
	}

}


void appTaskfu_10ms(void)
{
	static uint32 counter_before = 0;
	static uint32 counter_after = 0;
	static uint32 counter_gap = 0;

	task_cnt_10m++;

	/* every 10ms */
	LED_Buzzer();

	if(task_cnt_10m%2 == 0){	/* every 20ms */
		ultraRGB();
	}

	if(task_cnt_10m%5 == 0){	/* every 50ms */
		g_conversionResult = run_vadc();
		counter_before = g_Stm.counter_100M;
		convolution();
		counter_after = g_Stm.counter_100M;
		if(counter_after > counter_before) 	counter_gap = (counter_after - counter_before); //	printf("convolution counter: %d\n", (counter_after - counter_before));
		else 																counter_gap = (counter_after - counter_before + 100000000); //	printf("convolution counter: %d\n", (counter_after - counter_before + 100000000));
		// printf("convolution counter: %d\n", (counter_gap));
		__nop();		/* breaking point pseudo instruction  */
	}

	if(task_cnt_10m == 1000){
		task_cnt_10m = 0;
	}
}

void appTaskfu_100ms(void)
{
	task_cnt_100m++;
	if(task_cnt_100m == 1000){
		task_cnt_100m = 0;
	}

}

void appTaskfu_1000ms(void)
{
	task_cnt_1000m++;
	if(task_cnt_1000m == 1000){
		task_cnt_1000m = 0;
	}

}

void appTaskfu_idle(void){


}

void appIsrCb_1ms(void){

}



void LED_Buzzer(void)
{
	BUTTON1_State ^= (IfxPort_getPinState(BUTTON_SW1)== 0);
	BUTTON2_State ^= (IfxPort_getPinState(BUTTON_SW2)== 0);

    blink_LED(BUTTON1_State, BUTTON2_State,500, 50);       //ms   
}


unsigned int distance;

void ultraRGB(void)
{

    /* ultrasonic + RGB */
    static int max = 6250;

    PORT02_OMR |= (1<<PS7);            // Set LED RED
    PORT10_OMR |= (1<<PS3);            // Set LED BLUE


    if(distance < 30)                           // Only LED RED
    {
        GTM_TOM0_CH15_SR0 = 12500 - 1;
        GTM_TOM0_CH15_SR1 = max;                // Set LED RED DUTY
        GTM_TOM0_CH15_SR1 = 0;

        GTM_TOM0_CH3_SR0 = 12500 - 1;
        GTM_TOM0_CH3_SR1 = 0;                   // Set LED BLUE DUTY
    }
    else if(distance >= 30 && distance < 70 )   // Proportional to distance RED decrease, BLUE increase
    {
        GTM_TOM0_CH15_SR0 = 12500 - 1;
        GTM_TOM0_CH15_SR1 = max - 156.25*(distance - 30);       // 6250(max value) / 30(판별구간) = 156.25

        GTM_TOM0_CH3_SR0 = 12500 - 1;
        GTM_TOM0_CH3_SR1 = distance*156.25;
    }
    else if(distance >= 70)                     // Only LED BLUE
    {
        GTM_TOM0_CH15_SR0 = 12500 - 1;
        GTM_TOM0_CH15_SR1 = 0;

        GTM_TOM0_CH3_SR0 = 12500 - 1;
        GTM_TOM0_CH3_SR1 = max;
    }
    else
    {
        GTM_TOM0_CH15_SR0 = 12500 - 1;
        GTM_TOM0_CH15_SR1 = max;

        GTM_TOM0_CH3_SR0 = 12500 - 1;
        GTM_TOM0_CH3_SR1 = max;
    }        
}





#if INLINING == FALSE
void convolution()
#elif INLINING == TRUE
inline void convolution()
#endif		/* inlining */
{
#if MATRIX == INTEGER
	// inductive variable
	static int i = 0, j = 0, k = 0;
	static int out_x = 0, out_y = 0, out_ch = 0;
	static int partial_sum = 0, mul = 0;
#elif MATRIX == FLOATING
	// inductive variable
	static int i = 0, j = 0, k = 0;
	static int out_x = 0, out_y = 0, out_ch = 0;
	static float partial_sum = 0, mul = 0;
#endif

#if MAT_LOC == LOCAL
#if MATRIX == INTEGER	
#if DIMENSION == 2
	static int input[M][N];
	static int kernel[KERNEL_CH][P][Q];
	static int output[OUTPUT_CH][OUTPUT_X][OUTPUT_Y];
#elif DIMENSION == 3
	/*
	*  If not static, trap bus error occured at runtime.
	*	 inductive variables were corrupted after performed by the ISRs.
	*/
	int input[IN_CH][M][N];
	int kernel[KERNEL_CH][P][Q];
	int output[OUTPUT_CH][OUTPUT_X][OUTPUT_Y];
#endif		/* dimension */
#elif MATRIX == FLOAT	
	float input[IN_CH][M][N];
	float kernel[KERNEL_CH][P][Q];
	float output[OUTPUT_CH][OUTPUT_X][OUTPUT_Y];
#endif		/* matrix format */
#endif		/* matrix location */


#if DIMENSION == 2
	/* input & kernel initialization */
	// this should be repalced with the potentiometer data
	for(j = 0; j < M; j++)
		for(k = 0; k < N; k++)
			input[j][k] = 3;

	// assuming uniform filter, not trained at all.
	for(j = 0; j < P; j++)
		for(k = 0; k < Q; k++)
			kernel[j][k] = 1;
#elif DIMENSION == 3	
	/* input & kernel initialization */
	// this should be repalced with the potentiometer data
	for(i = 0; i < IN_CH; i++)
		for(j = 0; j < M; j++)
			for(k = 0; k < N; k++)
			{
#if MATRIX == INTEGER				
				input[i][j][k] = (uint32) g_conversionResult.B.RESULT;
				// input[i][j][k] = 3;		// constant value tested
#elif MATRIX == FLOATING				
				input[i][j][k] = (float32) g_conversionResult.B.RESULT;
				// input[i][j][k] = 3;		// constant value tested
#endif		/* matrix format */
			}

	// assuming uniform filter, not trained at all.
	for(i = 0; i < KERNEL_CH; i++)
		for(j = 0; j < P; j++)
			for(k = 0; k < Q; k++)
			{
#if MATRIX == INTEGER				
				kernel[i][j][k] = (uint32) g_conversionResult.B.RESULT;
				// kernel[i][j][k] = 1;
#elif MATRIX == FLOATING		
				kernel[i][j][k] = (float32) g_conversionResult.B.RESULT;
				// kernel[i][j][k] = 1;
#endif		/* matrix format */
			}
#endif		/* dimension */

#if OPTIMIZE_OPTION == DEFAULT
	for(out_ch = 0; out_ch < OUTPUT_CH; out_ch++) {				// an additional loop for RGB channels: 100 * 100 * 3
		for(out_x = 0; out_x < OUTPUT_X; out_x++){		// Assuming convolution a single channel input feature map with a single channel filter
			for(out_y = 0; out_y < OUTPUT_Y; out_y++){
				for(i = 0; i < IN_CH; i++){
					for (j = 0; j < Q; j++) {
						for (k = 0; k < P; k++) {
							mul = input[i][j+out_x][k+out_y] * kernel[i][k][j];
							partial_sum += mul;
							mul = 0;
						}
					}
				}	
				output[out_ch][out_x][out_y] = partial_sum;
				partial_sum = 0;
			}
		}
	}
#elif OPTIMIZE_OPTION == MADD
	for(out_ch = 0; out_ch < OUTPUT_CH; out_ch++) {				// an additional loop for RGB channels: 100 * 100 * 3
		for(out_x = 0; out_x < OUTPUT_X; out_x++){		// Assuming convolution a single channel input feature map with a single channel filter
			for(out_y = 0; out_y < OUTPUT_Y; out_y++){
				for(i = 0; i < IN_CH; i++){
					for (j = 0; j < Q; j++) {
						for (k = 0; k < P; k++) {
							partial_sum += input[i][j+out_x][k+out_y] * kernel[i][k][j];
						}
					}
				}	
				output[out_ch][out_x][out_y] = partial_sum;
				partial_sum = 0;
			}
		}
	}

#elif OPTIMIZE_OPTION == LOOP_UNROLL
	for(out_ch = 0; out_ch < OUTPUT_CH; out_ch++) {				// an additional loop for RGB channels: 100 * 100 * 3
		for(out_x = 0; out_x < OUTPUT_X; out_x++){		// Assuming convolution a single channel input feature map with a single channel filter
			for(out_y = 0; out_y < OUTPUT_Y; out_y++){
				for(i = 0; i < IN_CH; i++){
					for (j = 0; j < Q; j+=4) {
						for (k = 0; k < P; k+=4) {
							mul = input[i][j+out_x][k+out_y] * kernel[i][k][j];
							partial_sum += mul;
							mul = 0;
							mul += input[i][j+out_x][k+1+out_y] * kernel[i][k+1][j];
							partial_sum += mul;
							mul = 0;
							mul += input[i][j+out_x][k+2+out_y] * kernel[i][k+2][j];
							partial_sum += mul;
							mul = 0;
							mul += input[i][j+out_x][k+3+out_y] * kernel[i][k+3][j];
							partial_sum += mul;
							mul = 0;
							mul += input[i][j+1+out_x][k+out_y] * kernel[i][k][j+1];
							partial_sum += mul;
							mul = 0;
							mul += input[i][j+1+out_x][k+1+out_y] * kernel[i][k+1][j+1];
							partial_sum += mul;
							mul = 0;
							mul += input[i][j+1+out_x][k+2+out_y] * kernel[i][k+2][j+1];
							partial_sum += mul;
							mul = 0;
							mul += input[i][j+1+out_x][k+3+out_y] * kernel[i][k+3][j+1];
							partial_sum += mul;
							mul = 0;
							mul += input[i][j+2+out_x][k+out_y] * kernel[i][k][j+2];
							partial_sum += mul;
							mul = 0;
							mul += input[i][j+2+out_x][k+1+out_y] * kernel[i][k+1][j+2];
							partial_sum += mul;
							mul = 0;
							mul += input[i][j+2+out_x][k+2+out_y] * kernel[i][k+2][j+2];
							partial_sum += mul;
							mul = 0;
							mul += input[i][j+2+out_x][k+3+out_y] * kernel[i][k+3][j+2];
							partial_sum += mul;
							mul = 0;
							mul += input[i][j+3+out_x][k+out_y] * kernel[i][k][j+3];
							partial_sum += mul;
							mul = 0;
							mul += input[i][j+3+out_x][k+1+out_y] * kernel[i][k+1][j+3];
							partial_sum += mul;
							mul = 0;
							mul += input[i][j+3+out_x][k+2+out_y] * kernel[i][k+2][j+3];
							partial_sum += mul;
							mul = 0;
							mul += input[i][j+3+out_x][k+3+out_y] * kernel[i][k+3][j+3];
							partial_sum += mul;
							mul = 0;
						}
					}
				}	
				output[out_ch][out_x][out_y] = partial_sum;
				partial_sum = 0;
			}
		}
	}

#elif OPTIMIZE_OPTION == UNROLL_MADD
	for(out_ch = 0; out_ch < OUTPUT_CH; out_ch++) {				// an additional loop for RGB channels: 100 * 100 * 3
		for(out_x = 0; out_x < OUTPUT_X; out_x++){		// Assuming convolution a single channel input feature map with a single channel filter
			for(out_y = 0; out_y < OUTPUT_Y; out_y++){
				for(i = 0; i < IN_CH; i++){
					for (j = 0; j < Q; j+=4) {
						for (k = 0; k < P; k+=4) {
							partial_sum += input[i][j+out_x][k+out_y] * kernel[i][k][j];
							partial_sum += input[i][j+out_x][k+1+out_y] * kernel[i][k+1][j];
							partial_sum += input[i][j+out_x][k+2+out_y] * kernel[i][k+2][j];
							partial_sum += input[i][j+out_x][k+3+out_y] * kernel[i][k+3][j];
							partial_sum += input[i][j+1+out_x][k+out_y] * kernel[i][k][j+1];
							partial_sum += input[i][j+1+out_x][k+1+out_y] * kernel[i][k+1][j+1];
							partial_sum += input[i][j+1+out_x][k+2+out_y] * kernel[i][k+2][j+1];
							partial_sum += input[i][j+1+out_x][k+3+out_y] * kernel[i][k+3][j+1];
							partial_sum += input[i][j+2+out_x][k+out_y] * kernel[i][k][j+2];
							partial_sum += input[i][j+2+out_x][k+1+out_y] * kernel[i][k+1][j+2];
							partial_sum += input[i][j+2+out_x][k+2+out_y] * kernel[i][k+2][j+2];
							partial_sum += input[i][j+2+out_x][k+3+out_y] * kernel[i][k+3][j+2];
							partial_sum += input[i][j+3+out_x][k+out_y] * kernel[i][k][j+3];
							partial_sum += input[i][j+3+out_x][k+1+out_y] * kernel[i][k+1][j+3];
							partial_sum += input[i][j+3+out_x][k+2+out_y] * kernel[i][k+2][j+3];
							partial_sum += input[i][j+3+out_x][k+3+out_y] * kernel[i][k+3][j+3];
						}
					}
				}	
				output[out_ch][out_x][out_y] = partial_sum;
				partial_sum = 0;
			}
		}
	}
#elif OPTIMIZE_OPTION == LOOP_FACTORING
	static int* inp_fst = 0;
	static int* inp_snd = 0;
	static int* ker_fst = 0;

	for(out_ch = 0; out_ch < OUTPUT_CH; out_ch++) {				// an additional loop for RGB channels: 100 * 100 * 3
		for(out_x = 0; out_x < OUTPUT_X; out_x++){		// Assuming convolution a single channel input feature map with a single channel filter
			for(out_y = 0; out_y < OUTPUT_Y; out_y++){
				for(i = 0; i < IN_CH; i++){

					inp_fst = (input + i);
					ker_fst = (kernel + i);
					
					for (j = 0; j < Q; j++) {
						inp_snd = inp_fst + (j + out_x);
						for (k = 0; k < P; k++) {
							mul = *(inp_snd + (k + out_y)) * (*(ker_fst + k * j));
							partial_sum += mul;
							mul = 0;
						}
					}
				}	
				output[out_ch][out_x][out_y] = partial_sum;
				partial_sum = 0;
			}
		}
	}
#elif OPTIMIZE_OPTION == UNROLL_MADD_FAC
	static int* inp_fst;
	static int* inp_snd[P];
	static int* ker_fst;

	for(out_ch = 0; out_ch < OUTPUT_CH; out_ch++) {				// an additional loop for RGB channels: 100 * 100 * 3
		for(out_x = 0; out_x < OUTPUT_X; out_x++){		// Assuming convolution a single channel input feature map with a single channel filter
			for(out_y = 0; out_y < OUTPUT_Y; out_y++){
				for(i = 0; i < IN_CH; i++){

					inp_fst = (input + i);
					ker_fst = (kernel + i);
					
					for (j = 0; j < Q; j+=4) {
						inp_snd[j] = inp_fst + (j + out_x);
						inp_snd[j + 1] = inp_fst + (j + 1 + out_x);
						inp_snd[j + 2] = inp_fst + (j + 2 + out_x);
						inp_snd[j + 3] = inp_fst + (j + 3 + out_x);
						for (k = 0; k < P; k+=4) {
							partial_sum += *(inp_snd[j] + ((k) + out_y)) * (*(ker_fst + (k) * j));
							partial_sum += *(inp_snd[j] + ((k + 1) + out_y)) * (*(ker_fst + (k + 1) * j));
							partial_sum += *(inp_snd[j] + ((k + 2) + out_y)) * (*(ker_fst + (k + 2) * j));
							partial_sum += *(inp_snd[j] + ((k + 3) + out_y)) * (*(ker_fst + (k + 3) * j));
							partial_sum += *(inp_snd[j + 1] + ((k) + out_y)) * (*(ker_fst + (k) * (j + 1)));
							partial_sum += *(inp_snd[j + 1] + ((k + 1) + out_y)) * (*(ker_fst + (k + 1) * (j + 1)));
							partial_sum += *(inp_snd[j + 1] + ((k + 2) + out_y)) * (*(ker_fst + (k + 2) * (j + 1)));
							partial_sum += *(inp_snd[j + 1] + ((k + 3) + out_y)) * (*(ker_fst + (k + 3) * (j + 1)));
							partial_sum += *(inp_snd[j + 2] + ((k) + out_y)) * (*(ker_fst + (k) * (j + 2)));
							partial_sum += *(inp_snd[j + 2] + ((k + 1) + out_y)) * (*(ker_fst + (k + 1) * (j + 2)));
							partial_sum += *(inp_snd[j + 2] + ((k + 2) + out_y)) * (*(ker_fst + (k + 2) * (j + 2)));
							partial_sum += *(inp_snd[j + 2] + ((k + 3) + out_y)) * (*(ker_fst + (k + 3) * (j + 2)));
							partial_sum += *(inp_snd[j + 3] + ((k) + out_y)) * (*(ker_fst + (k) * (j + 3)));
							partial_sum += *(inp_snd[j + 3] + ((k + 1) + out_y)) * (*(ker_fst + (k + 1) * (j + 3)));
							partial_sum += *(inp_snd[j + 3] + ((k + 2) + out_y)) * (*(ker_fst + (k + 2) * (j + 3)));
							partial_sum += *(inp_snd[j + 3] + ((k + 3) + out_y)) * (*(ker_fst + (k + 3) * (j + 3)));
						}
					}
				}	
				output[out_ch][out_x][out_y] = partial_sum;
				partial_sum = 0;
			}
		}
	}

#endif		/* optimization options */

	// an nop instruction for breakpoint
	__nop();

}			/* end of convolution() */

#if 0
/* back-up code for 2D convolution operation */
for (i = 0; i < 3; i++) {	// an additional loop for RGB channels: 100 * 100 * 3
// Assuming convolution a single channel input feature map with a single channel filter
	for(out_x = 0; out_x < OUTPUT_X; out_x++){
		for(out_y = 0; out_y < OUTPUT_Y; out_y++){
				for (j = 0; j < Q; j++) {
					for (k = 0; k < P; k++) {
						// MAC unit usage
						partial_sum += input[j+out_x][k+out_y]*kernel[k][j];
					}
				}
			output[out_x][out_y] = partial_sum;
			partial_sum = 0;
		}
	}
}
#endif